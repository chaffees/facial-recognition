# Facial Recognition

This project demonstrates the use of third-party APIs and shows how to manipulate images using the Python imaging library (pillow), how to apply optical character recognition to images to recognize text using tesseract and py-tesseract, and how to identify faces in images using the popular opencv library.

## Getting Started

These instructions will get a copy of the project up and running on your local machine for development and testing.

## Prerequisites

    python >= 3.6
    zipfile library
    Pillow library
    tesseract-python library
    opencv-python library
    numpy library
    ipython toolkit

### Library Installation

    # pip install zipfile
    # pip install numpy
    # pip install opencv-pyhton
    # pip install pillow
    # pip install tesseract-python
    # pip install ipython

### Python & PIP Installation

See your package manager for supported ways to install the Python package.

Ex: Ubuntu

    # sudo apt update
    # sudo apt install python3
    # sudo apt install python3-pip
    # python3 --version
    # pip3 --version

### Running Project

1. Fork the project to your own repo.
2. Setup your local environment and pull repo local.
3. Change directory into project folder.
4. Run the project.py file.

## Authors

* **Scott Chaffee** [LinkedIn](https://www.linkedin.com/in/scott-chaffee-97144912/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE)
